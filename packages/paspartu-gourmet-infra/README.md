# paspartu-gourmet-infra

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build paspartu-gourmet-infra` to build the library.

## Running unit tests

Run `nx test paspartu-gourmet-infra` to execute the unit tests via [Jest](https://jestjs.io).
