import { Test } from '@nestjs/testing';
import { PaspartuGourmetInfraController } from './paspartu-gourmet-infra.controller';
import { PaspartuGourmetInfraService } from './paspartu-gourmet-infra.service';

describe('PaspartuGourmetInfraController', () => {
  let controller: PaspartuGourmetInfraController;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [PaspartuGourmetInfraService],
      controllers: [PaspartuGourmetInfraController],
    }).compile();

    controller = module.get(PaspartuGourmetInfraController);
  });

  it('should be defined', () => {
    expect(controller).toBeTruthy();
  });
});
