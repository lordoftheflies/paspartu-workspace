import { Test } from '@nestjs/testing';
import { PaspartuGourmetInfraService } from './paspartu-gourmet-infra.service';

describe('PaspartuGourmetInfraService', () => {
  let service: PaspartuGourmetInfraService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [PaspartuGourmetInfraService],
    }).compile();

    service = module.get(PaspartuGourmetInfraService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
