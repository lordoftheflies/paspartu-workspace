import { Controller } from '@nestjs/common';
import { PaspartuGourmetInfraService } from './paspartu-gourmet-infra.service';

@Controller('paspartu-gourmet-infra')
export class PaspartuGourmetInfraController {
  constructor(
    private paspartuGourmetInfraService: PaspartuGourmetInfraService
  ) {}
}
