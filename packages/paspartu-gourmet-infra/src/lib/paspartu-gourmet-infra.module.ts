import { Module } from '@nestjs/common';
import { PaspartuGourmetInfraController } from './paspartu-gourmet-infra.controller';
import { PaspartuGourmetInfraService } from './paspartu-gourmet-infra.service';

@Module({
  controllers: [PaspartuGourmetInfraController],
  providers: [PaspartuGourmetInfraService],
  exports: [PaspartuGourmetInfraService],
})
export class PaspartuGourmetInfraModule {}
