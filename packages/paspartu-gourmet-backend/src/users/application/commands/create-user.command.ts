import { Injectable, Inject } from '@nestjs/common';
import { UserModel } from '../../domain/user.model';
import { ICreateUserService } from '../../domain/services/create-user.service';
import { ICreateUserUsecase } from '../../infrastructure/usecases/create-user.usecase';

@Injectable()
export class CreateUserUsecase implements ICreateUserUsecase {
  constructor(@Inject() private userService: ICreateUserService) {}

  async create(user: UserModel): Promise<UserModel> {
    return this.userService.create(user);
  }
}
