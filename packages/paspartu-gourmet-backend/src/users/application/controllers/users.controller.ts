import {
  Controller,
  Inject,
  Post,
  Res,
  Body,
  HttpStatus,
  UsePipes,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Delete,
  ValidationPipe,
} from '@nestjs/common';
import { PartialUserModel, UserModel } from '../../domain/user.model';
import { ICreateUserUsecase } from '../../infrastructure/usecases/create-user.usecase';

@Controller('users')
export class UsersController {
  constructor(
    @Inject() private createUserApp: ICreateUserUsecase,
    @Inject() private getUserApp: IGetUserApplication,
    @Inject() private getAllUserApp: IGetAllUserApplication,
    @Inject() private editUserApp: IEditUserApplication,
    @Inject() private deleteUserApp: IDeleteUserApplication
  ) {}

  @UsePipes(new ValidationPipe())
  @Post('/create')
  async create(@Res() res, @Body() userDomain: UserModel) {
    try {
      const user = await this.createUserApp.create(userDomain);
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.CREATED,
        message: `${user.fullName} successfully created`,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json(err);
    }
  }

  @Get(':id')
  async findOne(@Res() res, @Param('id', new ParseUUIDPipe()) id) {
    try {
      const user = await this.getUserApp.getById(id);
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        data: user,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: err,
      });
    }
  }

  @Get()
  async findAll(@Res() res) {
    try {
      const users = await this.getAllUserApp.getAll();
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        data: users,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: err,
      });
    }
  }

  @Patch('/update/:id')
  async update(
    @Res() res,
    @Param('id', new ParseUUIDPipe()) id,
    @Body() data: PartialUserModel
  ) {
    try {
      const updatedUser = await this.editUserApp.update(id, data);
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        data: updatedUser,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: err,
      });
    }
  }

  @Delete('/delete/:id')
  async remove(@Res() res, @Param('id', new ParseUUIDPipe()) id: string) {
    try {
      await this.deleteUserApp.remove(id);
      return res.status(HttpStatus.OK).json({
        statusCode: HttpStatus.OK,
        message: 'User successfully deleted',
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        statusCode: HttpStatus.BAD_REQUEST,
        message: err,
      });
    }
  }
}
