import { UserModel } from "../../domain/user.model";

export interface ICreateUserUsecase {
    create(userDomain: UserModel): Promise<UserModel>;
}
