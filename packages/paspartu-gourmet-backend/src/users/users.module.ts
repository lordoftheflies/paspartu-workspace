import { Module } from '@nestjs/common';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [UsersController],
  providers: [
      createUserApp,
      getUserApp,
      getAllUserApp,
      editUserApp,
      deleteUserApp,
      createUserService,
      getUserService,
      getAllUserService,
      editUserService,
      deleteUserService
  ],
})
export class UsersModule {}
