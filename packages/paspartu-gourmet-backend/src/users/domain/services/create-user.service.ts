import { UserModel } from '../user.model';

export interface ICreateUserService {
    create(userModel: UserModel): Promise<UserModel>;
}
