

import { UserModel } from "../user.model";

export interface IEditUserService {
    update(id: string, data: UserModel): Promise<UserModel>
}
