import { UserModel, PartialUserModel } from "../models/user.model";

export interface EditUserService {
    update(id: string, data: PartialUserModel): Promise<UserModel>
}
