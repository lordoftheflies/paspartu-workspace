import { UserModel } from '../models/user.model';

export interface CreateUserService {
    create(userDomain: UserModel): Promise<UserModel>;
}
