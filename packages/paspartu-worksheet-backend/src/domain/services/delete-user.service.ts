export interface DeleteUserService {
  remove(id: string): Promise<{ deleted: boolean }>;
}
