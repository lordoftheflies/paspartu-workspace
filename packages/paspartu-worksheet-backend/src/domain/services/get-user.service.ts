import { UserModel } from "../models/user.model";

export interface GetUserService {
    getAll(): Promise<UserModel>
}
