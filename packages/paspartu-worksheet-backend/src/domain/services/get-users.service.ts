import { UserModel } from "../models/user.model";

export interface GetAllUserService {
    getAll(): Promise<UserModel[]>
}
