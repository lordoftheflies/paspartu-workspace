import { IsString, IsEmail } from 'class-validator';

export class UserModel {
  @IsString()
  readonly fullName: string;

  @IsString()
  readonly password: string;

  @IsEmail()
  readonly email: string;
}

export class PartialUserModel {
  @IsString()
  readonly fullName: string;

  @IsString()
  readonly password: string;

  @IsEmail()
  readonly email: string;
}
