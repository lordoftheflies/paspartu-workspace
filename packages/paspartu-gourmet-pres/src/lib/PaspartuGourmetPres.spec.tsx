import { render } from '@testing-library/react';

import PaspartuGourmetPres from './PaspartuGourmetPres';

describe('PaspartuGourmetPres', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<PaspartuGourmetPres />);
    expect(baseElement).toBeTruthy();
  });
});
