import styled from 'styled-components';

/* eslint-disable-next-line */
export interface PaspartuGourmetPresProps {}

const StyledPaspartuGourmetPres = styled.div`
  color: pink;
`;

export function PaspartuGourmetPres(props: PaspartuGourmetPresProps) {
  return (
    <StyledPaspartuGourmetPres>
      <h1>Welcome to PaspartuGourmetPres!</h1>
    </StyledPaspartuGourmetPres>
  );
}

export default PaspartuGourmetPres;
