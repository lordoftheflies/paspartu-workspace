# paspartu-gourmet-app

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build paspartu-gourmet-app` to build the library.

## Running unit tests

Run `nx test paspartu-gourmet-app` to execute the unit tests via [Jest](https://jestjs.io).
