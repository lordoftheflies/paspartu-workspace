import { Test } from '@nestjs/testing';
import { PaspartuGourmetAppController } from './paspartu-gourmet-app.controller';
import { PaspartuGourmetAppService } from './paspartu-gourmet-app.service';

describe('PaspartuGourmetAppController', () => {
  let controller: PaspartuGourmetAppController;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [PaspartuGourmetAppService],
      controllers: [PaspartuGourmetAppController],
    }).compile();

    controller = module.get(PaspartuGourmetAppController);
  });

  it('should be defined', () => {
    expect(controller).toBeTruthy();
  });
});
