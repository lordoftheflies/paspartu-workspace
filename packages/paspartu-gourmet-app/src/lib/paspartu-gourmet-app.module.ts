import { Module } from '@nestjs/common';
import { PaspartuGourmetAppController } from './paspartu-gourmet-app.controller';
import { PaspartuGourmetAppService } from './paspartu-gourmet-app.service';

@Module({
  controllers: [PaspartuGourmetAppController],
  providers: [PaspartuGourmetAppService],
  exports: [PaspartuGourmetAppService],
})
export class PaspartuGourmetAppModule {}
