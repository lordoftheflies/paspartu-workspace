import { Test } from '@nestjs/testing';
import { PaspartuGourmetAppService } from './paspartu-gourmet-app.service';

describe('PaspartuGourmetAppService', () => {
  let service: PaspartuGourmetAppService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [PaspartuGourmetAppService],
    }).compile();

    service = module.get(PaspartuGourmetAppService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
