import { Controller } from '@nestjs/common';
import { PaspartuGourmetAppService } from './paspartu-gourmet-app.service';

@Controller('paspartu-gourmet-app')
export class PaspartuGourmetAppController {
  constructor(private paspartuGourmetAppService: PaspartuGourmetAppService) {}
}
