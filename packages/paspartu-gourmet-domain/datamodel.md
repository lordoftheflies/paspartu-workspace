```mermaid
erDiagram

  nutritions {
    Int id PK 
    String code  
    String title  
    String description  "nullable"
    Float energy  "nullable"
    Float protein  "nullable"
    Float carboHydrate  "nullable"
    Float fiber  "nullable"
    Float fat  "nullable"
    Float glycemicIndex  "nullable"
    }
  

  Unit {
    Int id PK 
    String code  
    String title  
    String description  "nullable"
    }
  

  Ingredient {
    Int id PK 
    Float mass  
    Int unitId  
    Int recipeId  
    }
  

  Recipe {
    Int id PK 
    String code  
    String title  
    String description  "nullable"
    }
  

  Task {
    Int id PK 
    String code  
    String title  
    String description  "nullable"
    Int recipeId  
    Float duration  
    Int order  
    }
  

  Meal {
    Int id PK 
    String code  
    String title  
    String description  "nullable"
    DateTime timestamp  
    Float amount  
    Int recipeId  
    }
  

  Activity {
    Int id PK 
    String code  
    String title  
    String description  "nullable"
    Float intensity  "nullable"
    }
  

  Workout {
    Int id PK 
    String code  
    String title  
    String description  "nullable"
    DateTime timestamp  
    Float duration  
    Int workoutPlanId  "nullable"
    Int activityId  "nullable"
    }
  

  Excercise {
    Int id PK 
    String code  
    String title  
    String description  "nullable"
    String video  "nullable"
    Int frequency  "nullable"
    Int serie  "nullable"
    Float duration  "nullable"
    Int workoutPlanId  
    }
  

  WorkoutPlan {
    Int id PK 
    String code  
    String title  
    String description  "nullable"
    }
  
    Ingredient o{--|| Unit : "unit"
    Ingredient o{--|| Recipe : "recipe"
    Task o{--|| Recipe : "recipe"
    Meal o{--|| Recipe : "recipe"
    Workout o{--|o WorkoutPlan : "WorkoutPlan"
    Workout o{--|o Activity : "Activity"
    Excercise o{--|| WorkoutPlan : "workoutPlan"
```
