# paspartu-gourmet-domain

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build paspartu-gourmet-domain` to build the library.

## Running unit tests

Run `nx test paspartu-gourmet-domain` to execute the unit tests via [Jest](https://jestjs.io).
