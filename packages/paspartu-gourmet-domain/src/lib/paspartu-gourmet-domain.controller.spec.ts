import { Test } from '@nestjs/testing';
import { PaspartuGourmetDomainController } from './paspartu-gourmet-domain.controller';
import { PaspartuGourmetDomainService } from './paspartu-gourmet-domain.service';

describe('PaspartuGourmetDomainController', () => {
  let controller: PaspartuGourmetDomainController;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [PaspartuGourmetDomainService],
      controllers: [PaspartuGourmetDomainController],
    }).compile();

    controller = module.get(PaspartuGourmetDomainController);
  });

  it('should be defined', () => {
    expect(controller).toBeTruthy();
  });
});
