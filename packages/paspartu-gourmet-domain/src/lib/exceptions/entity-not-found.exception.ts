export  class EntityNotFoundException<T> extends Error {

  constructor(entityType: string, entityId: T) {
    super(`${entityType} with ID=${entityId} not found`);
    this.name = 'EntityNotFoundException';
  }
}
