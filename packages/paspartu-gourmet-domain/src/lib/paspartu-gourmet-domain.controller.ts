import { Controller } from '@nestjs/common';
import { PaspartuGourmetDomainService } from './paspartu-gourmet-domain.service';

@Controller('paspartu-gourmet-domain')
export class PaspartuGourmetDomainController {
  constructor(
    private paspartuGourmetDomainService: PaspartuGourmetDomainService
  ) {}
}
