import { Test } from '@nestjs/testing';
import { PaspartuGourmetDomainService } from './paspartu-gourmet-domain.service';

describe('PaspartuGourmetDomainService', () => {
  let service: PaspartuGourmetDomainService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [PaspartuGourmetDomainService],
    }).compile();

    service = module.get(PaspartuGourmetDomainService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
