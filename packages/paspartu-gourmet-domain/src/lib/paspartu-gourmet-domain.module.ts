import { Module } from '@nestjs/common';
import { PaspartuGourmetDomainController } from './paspartu-gourmet-domain.controller';
import { PaspartuGourmetDomainService } from './paspartu-gourmet-domain.service';

@Module({
  controllers: [PaspartuGourmetDomainController],
  providers: [PaspartuGourmetDomainService],
  exports: [PaspartuGourmetDomainService],
})
export class PaspartuGourmetDomainModule {}
